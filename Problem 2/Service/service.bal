
import ballerina/grpc;

listener grpc:Listener ep = new (9091);

@grpc:ServiceDescriptor {
    descriptor: ROOT_DESCRIPTOR,
    descMap: getDescriptorMap()
}
service "Repo" on ep {

    remote function add_new_fn(metadata functionInfo) returns string|error {

        string funVersion = functionInfo.Version;

        addMetaData(funVersion, functionInfo);

        string payload = "Status : function created; function version : " + funVersion;

        return payload;
    }

    remote function add_fns(stream<metadata> functionInfo) returns string|error {
        string[] createdVersions = [];

        foreach var item in functionInfo {
            string funVersion = item.Version;
            createdVersions.push(funVersion);
            addMetaData(funVersion, item);
        }

        string payload = "Status : functions created: " + createdVersions.toJsonString();

        return payload;
    }

    remote function delete_fn(string funVersion) returns string|error {
        string payload;
        json result = getJson(funVersion);
        deleteMetaData(funVersion);

        payload = "function version : '" + funVersion + "' removed.";

        return payload;
    }

    remote function show_fn(string funVersion) returns (string|error|json) {
        string payload = "";
        json Result = {};
        Result = getMetadata(funVersion);

        return Result;
    }

    remote function show_all_fns(string funVersion) returns string|error|stream {
        stream Result = getAllMetadata();

        return Result;
    }

    remote function show_all_with_criteria(RepoStringCaller caller, stream<metadata, error?> clientStream) returns error? {
        check clientStream.forEach(function(metadata funVersion) {
            checkpanic caller->sendString(string `Version: ${funVersion.Version}`);
        });

        check caller->complete();
    }
}



isolated map<metadata> funtionMetadata = {};

isolated function addMetaData(string funVersion, metadata functionInfo) {
    lock {
        funtionMetadata[funVersion] = functionInfo.clone();
    }
}

isolated function deleteMetaData(string funVersion) {
    lock {
        _ = funtionMetadata.remove(funVersion);
    }
}

isolated function getJson(string funVersion) returns json {
    lock {
        return funtionMetadata.hasKey(funVersion);
    }
}

isolated function getMetadata(string funVersion) returns json {
    lock {
        return funtionMetadata[funVersion].clone();
    }
}

isolated function getAllMetadata() returns stream {
    lock {
        return funtionMetadata.clone().toArray().toStream();
    }
}


